# DFStream - Protocol

## Description

_DFStream_ or _Distributed File Stream_ is right now an idea how to distribute large files to `n` servers simultaneously __without__ copying it `n` times but __one__ time.

This repository only covers the protocol details like protobuf files, sequence diagrams and further details.

## Roadmap

TBD

## Contributing

TBD

## Authors and acknowledgment
