# Protocol draft

## Overview

A global overview of the protocol is described in the following sequence diagram:

```mermaid
sequenceDiagram
  participant ClientA
  participant MulticastClients
  participant Operator
  participant Server
  ClientA->>+Server: Init session for File FileA (gRPC)
  Server->>Server: Open random UPD multicast port
  Server->>Server: Generate cipher suite session key
  Server-->>-ClientA: Session details (gRPC)
  MulticastClients->>Server: Connect to multicast socket (UDP)
  opt other clients register
  loop other clients
  MulticastClients ->>+Server: List sessions
  Server -->-MulticastClients: Session list with details
  MulticastClients ->>Server: Connect to multicast socket (UDP)
  end
  end

  opt Operator
  Operator->>Server: Start streaming (gRPC)
  end

  opt All clients registered
  Server-->Server: Expected number of clients registered
  end

  opt Minimum number clients registered
  Server-->Server: At least number of clients already registered
  end

  loop stream file - up to window size packets
  Server->>+MulticastClients: Send encrypted file chunk (UDP)
  opt Packet timeout
  Server->>+Server: Packet exceeds timeout
  Server->>MulticastClients: Resend packet (UDP)
  end
  MulticastClients->>MulticastClients: Handle packet
  MulticastClients-->-Server: Ack packet received (UDP)
  end
  Server->>MulticastClients: Send stream summary (gRPC)
```
## Terms

To clarify some terms in the above sequence diagram the following sections explain the basics and give some background.

### Server

The server part consists of static gRPC API that is always available and that implements the `Streams` service described in the [streams.proto](proto/streams/v1/streams.proto).

The `Streams` service defines the following calls:

- `ListSessions` - get a list of all available sessions - completed sessions are not included. `ListSessions` only returns the ID and the name of the file that should be streamed. The concept of a `Session` is described later on.
- `InitSession` - initialize a new session to stream a file with a given cipher suite and encoding. The server responds with an error if the given cipher suite or encoding is not supported. Setting a `session_timeout` allows to automatically remove sessions that where never started __and__ where no client is subscribed.
- `CloseSession` - if a session got initialized but no client subscribed to it it can be closed to free the UDP port and other allocated resources.
- `StreamSession` - which is a bidirectional gRPC streaming call. As soon as a client joins a stream session of a given `session_id` a corresponding response is sent including the necessary session details. If a client sends no `session_id` and there is only one active session the server might decide to subscribe the client to the only available session. A session is considered to be closed when __all__ clients disconnected from `StreamSession` or all chunks were acknowledged and the timeout after last chunk exceeded.

### Client

- clients picks its own ID?

### Session

- remove client when chunk reaches `max_retries`

### Chunk

Total size of the payload within the chunk should be ~400B.

It consists of the following components:

- 16-bit Big endian encoded unsigned integer size of encrypted content
- Encrypted Protobuf content

The layout of a chunk is:

```
-----------------------------------------------
| Size indicator | Encrypted Protobuf content |
-----------------------------------------------
```

No special split escape sequence between size indicator and encrypted protobuf content required.

The total size of the Protobuf content might vary due to some factors:

- A chunk contains a `int64 sequence_id` that is encoded as so called varint in Protobuf. That means it only contains the necessary bits to encode the number.
- A chunk contains a `int64 offset` that is also encoded as varint and therefore also might change in size the more data is sent.
